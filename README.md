[![PureScript](logo.png)](http://purescript.org)

Experimantal Fork of [Purescript](https://purescript.org) with a PHP-Backend.

Current State
-----

- [x] Port: Prelude, Arrays, BiFunctors, Console, Control, Debug, Distributive, Effect, Either, Foldable-Traversable, Identity, Invariant, Maybe, Newtype, NonEmpty, Orders, Partial, Refs, ST, TailRec, Tuples, Type-Equality, Unfoldable, Unsafe-Coerce (ST packages not yet fully functional)
- [x] Test Ports
- [X] Port "/tests" folder [500 out of 506 passing](https://gitlab.com/purescript-php/purescript-php/-/jobs/78783930)
- [X] Move packages into a psc-package set. [here](https://gitlab.com/purescript-php/package-sets-phpurs)
- [ ] move from associative Arrays to Objects for the ability to change paramenters passed as values
- [ ] PHP-HTTP FFI (using [ReactPHP](https://reactphp.org/)?)
- [ ] PHP-SQL FFI
- [ ] Hyper port?
- [ ] Test with PHP 5?

Basic Usage
-------

### Installing 

[Download](https://gitlab.com/purescript-php/purescript-php/-/jobs/78783929/artifacts/file/purescript-0.12.0/.stack-work/install/x86_64-linux/lts-11.7/8.2.2/bin/purs) and copy the compiler-binary into `~/.local/bin` or `/usr/local/bin` named `phpurs`

`phpurs` can also be compiled and tested in the same way as the original project.

### Getting Started

after installing:
```bash
$ mkdir my-project
$ pulp --psc-package init
```

change the `psc-package.json` file to:
```json
{
  "name": "my-project",
  "set": "psc-0.12.0",
  "source": "https://gitlab.com/purescript-php/package-sets-phpurs.git",
  "depends": [
    "psci-support",
    "console",
    "effect",
    "prelude"
  ]
}
```

### Compiling

```bash
$ phpurs compile src/*.purs $(psc-package sources)
```



### Calling

create a file `runMain.php` containing:
```php
require(dirname(__FILE__) . "/output/Main/index.php");
$PS__Main["main"]([]);
```

run with:
```bash
$ php -f runMain.php
```

FFI
---

Foreign are imported the same way, as in Purescript:
```
-- MyModule.purs
module MyModule where

foreign myForeign :: String -> Int -> Int
```

on the PHP-Side:
```php
// MyModule.php
$exports['myForeign'] = function($s) {
	return function ($n) use (&$s) {
		return $s . "test";
	}
}
```
it is aimed a being as close a possible to the JS version, to make porting code easier.

### FFI-Gotchas

- php does call by value. If you need to change the value, that is passed in you need to use `&$var` instead of `$var`.
- `&$var` *fails* if the caller provides a literal:
	```php
	$f = function($a, $b) {return $a+$b};
	$f(1, 2); // throws a error, since 1 cannot be referenced
	```
	> is should be possible to fix this at a compiler level. But that is not implemented (yet)
