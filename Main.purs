module Main where

import Debug.Trace
import Effect
import Effect.Console
import Prelude

import Control.Monad.ST as ST
import Control.Monad.ST.Ref as STRef

data TestData = A Int | B
newtype TestNewtype = TestNewtype String

testNewtype = TestNewtype "asd"

a = A
b = B

testAB x (TestNewtype s) = do
	log (case x of
		A i -> "asd" <> s
		B -> "bsd" <> s
	)

main = do
	_ <- testAB (A 2) testNewtype
	void $ testAB (B) testNewtype

-- test :: Int -> Int
-- test n = ST.run (do
-- 	r <- STRef.new n
-- 	count <- STRef.new 0
-- 	ST.while (map (_ >= 1) (STRef.read r)) do
-- 		_ <- STRef.modify (_ + 1) count
-- 		m <- STRef.read r
-- 		void $ STRef.write (m - 1) r
-- 	STRef.read count)

-- main = do
-- 	log "\nTrue:"
-- 	logShow $ 2 > 1
-- 	logShow $ 1.2 > 1.1
-- 	logShow $ -1 <= 0
-- 	log "\nFalse:"
-- 	logShow $ -10 > 1
-- 	logShow $ -10 > (1/2)
-- 	logShow $ test 10
-- 	log $ spy "skyMsg" "Done"
