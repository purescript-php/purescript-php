<?php

$exports['merge'] = function ($dict) {
  return function ($l) use (&$dict) {
    return function ($r) use (&$dict, &$l) {
      return array_merge($r, $l);
      // var o = {};
      // return Object.assign(o, r, l);
    };
  };
};
