module Main where

import Prelude
import Effect
import Effect.Console

head :: forall a. Partial => Array a -> a
head [x] = x

main :: Effect _
main = case (head [1, 2]) of 
	1 -> log "Done"
	_ -> log "Fail"
