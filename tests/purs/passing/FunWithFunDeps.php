<?php
//: forall e. FVect Z e
$exports['fnil'] = [];

//: forall n e. e -> FVect n e -> FVect (S n) e
$exports['fcons'] = function ($hd) {
  return function ($tl) use (&$hd) {
    return array_merge([$hd], $tl);
  };
};

$exports['fappend'] = function ($dict) {
  return function ($left) {
    return function ($right) use (&$left) {
      return array_merge($left, $right);
    };
  };
};

$exports['fflatten'] = function ($dict) {
  return function ($v) {
    return array_merge(... $v);
  };
};

$exports['ftoArray'] = function ($vect) {
  return $vect;
};
