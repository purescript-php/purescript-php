<?php

$exports['unsafeGet'] = function ($s) {
  return function ($o) use (&$s) {
    return $o[$s];
  };
};

$exports['unsafeSet'] = function($s) {
  return function($a) use (&$s) {
    return function ($o) use (&$a, &$s) {
      $o1 = [];
      $o1[$s] = $a;
      return array_merge($o, $o1);
      // return Object.assign({}, o, o1);
    };
  };
};
